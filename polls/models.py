from __future__ import unicode_literals
import datetime

from django.db import models
from django.utils import timezone







class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.question_text


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now

    def __str__(self):
        return self.choice_text

# Create your models here.
class Name(models.Model):
    name_value = models.CharField(max_length=100)

    def __str__(self): # if Python 2 use __unicode__
        return self.name_value



class Nosotros(models.Model):
    titulo = models.CharField(max_length=250, null=True, verbose_name="Título")
    description = models.TextField(max_length=700, verbose_name="Descripción")
    imagen = models.ImageField(upload_to = 'nosotros/',verbose_name="Imagen")
    
    direccion = models.CharField(max_length=80, null=True, blank=True, verbose_name="Dirección")
    tel = models.IntegerField(null= True,blank=True, verbose_name="Teléfono")
    mail = models.EmailField(null= True,blank=True, verbose_name="Correo")
    instagram = models.URLField(null=True,blank=True,verbose_name="Instagram")
    facebook= models.URLField(null=True,blank=True, verbose_name="Facebook")
    skype= models.URLField(null=True,blank=True, verbose_name="Skype")
    linkedin= models.URLField(null=True,blank=True, verbose_name="Linkedin")
    twitter= models.URLField(null=True,blank=True, verbose_name="Twitter")
    

    class Meta:
        verbose_name = 'Nos'
        verbose_name_plural = 'Nosotros'

    def __str__(self):
        return str(self.titulo)


class Especialidad(models.Model):
    titulo = models.CharField(max_length=250, null=True, verbose_name="Título")
    description = models.TextField(max_length=700, verbose_name="Descripción")
    imagen = models.ImageField(upload_to = 'nosotros/',verbose_name="Imagen")
    
   
    class Meta:
        verbose_name = 'Especialidad'
        verbose_name_plural = 'Especialidad'

    def __str__(self):
        return str(self.titulo)


class Citados(models.Model):
    mail = models.EmailField(null= True,blank=True, verbose_name="Correo")
    contraseña = models.TextField(max_length=700, verbose_name="Contraseña")
    fecha = models.DateTimeField(auto_now_add=True, verbose_name="fecha")
    
   
    class Meta:
        verbose_name = 'Citados'
        verbose_name_plural = 'Citados'

    def __str__(self):
        return str(self.email)
